# Firefox Grounded userChrome

A userChrome for Firefox that lowers tabs' position to make them attached to the bottom.

## Contents:
  - [Images](#images)
  - [Installation](#installation)
  - [Issues](#issues)

## Images:
**General:**

<img src="/Images/main.png" alt="Main image of theme with custom css" width="80%"/>

**Tab:**

![Main image of theme with custom css](/Images/tab.png)


## Installation
  - [Download chrome folder](https://gitlab.com/57d/firefox-grounded-userchrome/-/archive/main/firefox-grounded-userchrome-main.zip?path=chrome)
  - Open firefox
  - Open address bar and go to `about:config`, press "Accept the Risk and Continue"
  - Search for `toolkit.legacyUserProfileCustomizations.stylesheets` and double click to change to `true`
  - Open address bar and go to `about:support`
  - Find `Profile Folder` line and press `Open Folder`(or `Profile Directory` and `Open Directory`)
  - Copy the `chrome` folder here
  - Restart firefox

## Issues

I have tested userchrome on Manjaro and EndeavourOS GNU/Linux, KDE, so I am not sure if everything works correctly on other systems. If you find any issue, please describe it [here](https://gitlab.com/57d/firefox-grounded-userchrome/-/issues/new)
